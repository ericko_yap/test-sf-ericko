NOTES
- Code created by Ericko Yaputro
- Please change the input file string on app.js on variable "inputFile"

Test Cases:
1. PLACE COMMAND
	a. No "PLACE" command and do MOVE, LEFT, RIGHT, REPORT
		- Expected result: "" (output empty)
		
	b. "PLACE" command is out of bounds, and do MOVE, LEFT, RIGHT, REPORT. Input: "PLACE 10,9,EAST"
		- Expected result: "" (output empty)
		
	c. PLACE 5,5,EAST, then REPORT
		- Expected Result: 5,5,EAST
		
	d. First PLACE command shows after other commands, then REPORT
		- Expected Result: 1,2,WEST
		
	e. Multiple PLACE commands throughout input
		- Expected Result: 4,5,EAST
		
	f. PLACE after REPORT
		- Expected Result: 3,4,SOUTH
		
	g. Incorrect PLACE data
		- Expected Result: "ERROR: Incorrect "PLACE" command data format. Example: "PLACE 1,2,NORTH". Facing must be either NORTH, SOUTH, EAST, or WEST"
	
2. MOVE COMMAND
	a. MOVE without PLACE first
		- Expected result: "" (output empty)
	
	b. MOVE facing EAST from "1,2,EAST"
		- Expected result: 2,2,EAST
		
	c. MOVE facing NORTH from "1,2,NORTH"
		- Expected result: 1,3,NORTH
		
	d. MOVE facing WEST from "1,2,WEST"
		- Expected result: 0,2,WEST
		
	e. MOVE facing SOUTH from "1,2,SOUTH"
		- Expected result: 1,1,SOUTH
		
	f. MOVE on edge x = width from "5,0,EAST"
		- Expected Result: 5,0,EAST
		
	g. MOVE on edge x = 0 from "0,3,WEST"
		- Expected Result: 0,3,WEST
		
	h. MOVE on edge y = height from "2,5,NORTH"
		- Expected Result: 2,5,NORTH
		
	i. MOVE on edge y = 0 from "4,0,SOUTH"
		- Expected Result: 4,0,SOUTH
	
3. LEFT COMMAND
	a. LEFT without PLACE first
		- Expected result: "" (output empty)
		
	b. LEFT when facing NORTH
		- Expected result: 1,2,WEST
		
	c. LEFT when facing WEST
		- Expected result: 1,2,SOUTH
		
	d. LEFT when facing SOUTH
		- Expected result: 1,2,EAST
		
	e. LEFT when facing EAST
		- Expected result: 1,2,NORTH
		
	f. LEFT 4 times, start facing NORTH
		- Expected result: 1,2,NORTH

4. RIGHT COMMAND
	a. RIGHT without PLACE first
		- Expected result: "" (output empty)
		
	b. RIGHT when facing NORTH
		- Expected result: 1,2,EAST
		
	c. RIGHT when facing WEST
		- Expected result: 1,2,NORTH
		
	d. RIGHT when facing SOUTH
		- Expected result: 1,2,WEST
		
	e. RIGHT when facing EAST
		- Expected result: 1,2,SOUTH
		
	f. RIGHT 4 times, start facing NORTH
		- Expected result: 1,2,NORTH
	
5. REPORT COMMAND
	a. REPORT without PLACE first
		- Expected result: "" (output empty)
		
	b. REPORT after PLACE 5,0,EAST
		- Expected result: 5,0,EAST
		
	c. REPORT after all commands are inputted.
		- Expected result: 2,3,EAST
		
	d. REPORT after REPORT
		- Expected result: 4,0,SOUTH (only one row)
		
6. OTHER
	a. Command other than PLACE, MOVE, LEFT, RIGHT, REPORT
		- Expected result: "ERROR: Unknown command"
		
	b. Load non-existent file (change random string on app.js)
		- Expected result: "ERROR: Input file not found"