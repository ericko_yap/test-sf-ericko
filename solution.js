const fs = require('fs');

// create module
exports.getSolution = function(inputFile){
    // set board width & height
    board = initBoard(5,5);

    // read input file, then pass it to executeCommands() to run all the commands
    fs.readFile(inputFile, 'utf8', function(err, data) {
        if (!err){
            executeCommands(data);
            console.log(resultOutput);
        }
        else {
            console.log('ERROR: Input file not found!');
        }
    });
};

// initialize board, width & height
let board = {
    width: 5,
    height: 5
};

// initialize robot position and facing.
let robot = {
    pos: {x: 0, y: 0},
    facing: 'NORTH'
};

// list directions the robot will be facing
const mapFacing = ['NORTH', 'EAST', 'SOUTH', 'WEST'];

// to check if first PLACE command is already found & valid.
let placeValid = false;
let resultOutput = '';

// run all commands parsed from input string from file.
function executeCommands(input){
    const inputArray = input.split('\r\n');
    for (let a = 0; a < inputArray.length; a++){
        const cmd = inputArray[a];
        const data = cmd.split(' ');
        const res = command(data[0], data[1] ? data[1] : '', a+1);
        if (!res){
            break;
        }
    }
}

// initialize board width & height
function initBoard(width, height){
    board = {
        width: width,
        height: height
    };
    return board;
}

function checkInitData(initData){
    let data = initData.split(',');
    const checkX = Number.isInteger(data[0] - '0');
    const checkY = Number.isInteger(data[1] - '0');
    const checkFacing = mapFacing.indexOf(data[2]) > -1;

    return checkX && checkY && checkFacing;
}

// parse each command and separate to each functions.
function command(cmd, initData, idx){
    if (cmd === 'PLACE'){
        // check if initData format is correct
        if (checkInitData(initData)){
            return cmdPlace(initData);
        }
        else {
            console.log('ERROR: Incorrect "PLACE" command data format. Example: "PLACE 1,2,NORTH". Facing must be either NORTH, SOUTH, EAST, or WEST');
        }

    }

    if (placeValid) {
        switch (cmd) {
            case 'MOVE':
                return cmdMove();
                break;
            case 'LEFT':
                return cmdRotate(-1);
                break;
            case 'RIGHT':
                return cmdRotate(1);
                break;
            case 'REPORT':
                const report = cmdReport();

                // if after report has PLACE again, can output multiple
                resultOutput += report.message + '\n';

                // if place is valid.
                placeValid = false;
                return report;
                break;
            default:
                // return error if command not found.
                console.log("ERROR: Unknown COMMAND '" + cmd + "' on line: " + idx);
                return false;
                break;
        }
    }

    // enable this code to show error if "PLACE" command has not been called.
    // console.log("Error: init with command 'PLACE' has not been called yet.");
    return {
        result: "fail",
        message: "ERROR: Init with command 'PLACE' has not been called yet."
    }
}

// place the robot on a board, if out of bounds is not valid.
function cmdPlace(initData){
    let pos = initData.split(',');
    if (pos.length == 3) {
        const checkInitX = pos[0] >= 0 && pos[0] <= board.width;
        const checkInitY = pos[1] >= 0 && pos[1] <= board.height;

        // assign robot initial position
        robot.pos.x = checkInitX ? pos[0] - '0' : robot.pos.x;
        robot.pos.y = checkInitY ? pos[1] - '0' : robot.pos.y;
        robot.facing = checkInitX && checkInitY ? pos[2] : robot.facing;

        // check whether PLACE input is not out of bounds
        placeValid = checkInitX && checkInitY;

        return {
            result: (checkInitX && checkInitY ? 'success' : 'fail'),
            message: (checkInitX && checkInitY ? 'Success' : 'Fail')
        };
    }

    return {
        result: 'fail',
        message: 'Wrong data to be parsed'
    }
}

// move robot, according to which direction its facing.
function cmdMove(){
    switch(robot.facing){
        case 'NORTH': return moveRobot('y',1);
        case 'SOUTH': return moveRobot('y',-1);
        case 'EAST': return moveRobot('x', 1);
        case 'WEST': return moveRobot('x', -1);
    }
}

// change robot location depending on which direction its facing.
function moveRobot(axis, step){
    switch(axis){
        case 'x':
            const resX = robot.pos.x + step;

            // make sure the move still make the robot on the board if going EAST or WEST
            if (resX <= board.width && resX >= 0) {
                robot.pos.x += step;
                return { result: 'success' };
            }
            break;
        case 'y':
            const resY = robot.pos.y + step;

            // make sure the move still make the robot on the board if going NORTH or SOUTH
            if (resY <= board.height && resY >= 0){
                robot.pos.y += step;
                return { result: 'success' };
            }
            break;
    }

    return { result: 'fail' };
}

// rotate robot direction, made sure the facing map is looping.
function cmdRotate(rotateIdx){

    // make a looping facing direction with array
    rotateIdx = rotateIdx < 0 ? -1 : (rotateIdx > 0 ? 1 : 0);
    let idx = mapFacing.indexOf(robot.facing);
    idx = (idx + rotateIdx) % mapFacing.length;
    idx = idx < 0 ? mapFacing.length - 1 : idx;
    robot.facing = mapFacing[idx];

    return { result: 'success', message: 'success' };
}

// return message that the robot will show its position.
function cmdReport(){
    return {
        result: 'success',
        message: robot.pos.x  + ',' + robot.pos.y + ',' + robot.facing
    };
}

